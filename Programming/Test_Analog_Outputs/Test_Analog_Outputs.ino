#include <stdio.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 10, 8, 9, 7);

void setup()
{
lcd.begin(16, 2);  
}

void loop()
{
  int lapseOff  = 10;
  //int lapseOn = 10;
  int time = millis()/1000;
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(time);
  int a = 1;
  
  if(time == (lapseOff*a)){
    a = a + 2;
    int b = 1;
    int lapseOn = time - (10*b);
    while (lapseOn < 10*b){
      b = b + 1;
      lapseOn = (millis()/1000) - (10);
      
  lcd.setCursor(0, 0);
  // print the number of seconds since reset:
  lcd.print(lapseOn);
  //Codigo para controlar los motores
  //Definicion pines digitales 
  int pin0 = A4;
  int pin1 = A5;
  int pin2 = 2;
  int pin3 = 3;
  //Definicion pines analogos   
  int pin4 = A0;
  int pin5 = A1;
  int pin6 = A2;
  int pin7 = A3;
  //Definicion Pin Relay
  int relay = 1;
  
  //Definicion pines digitales como salida
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(relay, OUTPUT);
  
  //Defnicion pines analogos como salida
  pinMode(pin4, OUTPUT); 
  pinMode(pin5, OUTPUT);
  pinMode(pin6, OUTPUT);
  pinMode(pin7, OUTPUT);
  
  digitalWrite(relay, LOW);

    
  //Secuencia de movimiento motor 1
  digitalWrite(pin0, HIGH); // Primer paso
  digitalWrite(pin4, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin5, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin3, LOW);
  digitalWrite(pin7, LOW);
  delay(2);
  
  digitalWrite(pin0, LOW); // Segundo paso
  digitalWrite(pin4, LOW);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin5, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin3, LOW);
  digitalWrite(pin7, LOW);
  delay(2);
  
  digitalWrite(pin0, LOW); // Tercer paso
  digitalWrite(pin4, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin5, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin6, HIGH);
  digitalWrite(pin3, LOW);
  digitalWrite(pin7, LOW);
  delay(2);
  
  digitalWrite(pin0, LOW); // Cuarto paso
  digitalWrite(pin4, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin5, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin3, HIGH);
  digitalWrite(pin7, HIGH);
  delay(2);
  digitalWrite(relay, HIGH);
    }
  }
  

  /*
  digitalWrite(pin0, LOW); // Segundo paso
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
  delay(10);
  
  digitalWrite(pin0, LOW); // Tercer paso
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin3, LOW);
  delay(10);
  
  digitalWrite(pin0, LOW); // Cuarto paso
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, HIGH);
  delay(10);  
 
  //Secuencia de movimiento motor 2
  digitalWrite(pin4, HIGH); // Primer paso
  digitalWrite(pin5, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin7, LOW);
  delay(10);
  
  digitalWrite(pin4, LOW); // Segundo paso
  digitalWrite(pin5, HIGH);
  digitalWrite(pin6, LOW);
  digitalWrite(pin7, LOW);
  delay(10);
  
  digitalWrite(pin4, LOW); // Tercer paso
  digitalWrite(pin5, LOW);
  digitalWrite(pin6, HIGH);
  digitalWrite(pin7, LOW);
  delay(10);
  
  digitalWrite(pin4, LOW); // Cuarto paso
  digitalWrite(pin5, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin7, HIGH);
  delay(10);
  */

}
