// include the libraries
#include <LiquidCrystal.h>
#include <DHT22.h>
#include <stdio.h>
#define DHT22_PIN 2

// Setup a DHT22 instance
DHT22 myDHT22(DHT22_PIN);

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 10, 8, 9, 7);
int sensorPir = 3;
int buzzer =  5;
int fan = 6;
int resistor = 4;
//Definicion pines motor 2
int pin4 = A0;
int pin5 = A1;
int pin6 = A2;
int pin7 = A3;
//Definicion pines motor1
int pin0 = A4;
int pin1 = A5;
int pin2 = 0;
int pin3 = 1;
//Defining Function Times of Motor
int motorDelayTime = 3;
int motorState = LOW;             // ledState used to set the LED
long previousMillis = 0;        // will store last time LED was updated


void setup()
{
  // start serial port for temperature and humidity sensor
  Serial.begin(9600);

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Definicion de pines como entradas o salidas
  pinMode(sensorPir, INPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(fan, OUTPUT);
  pinMode(resistor, OUTPUT);

  //Definicion pines digitales como salida
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);

  //Defnicion pines analogos como salida
  pinMode(pin4, OUTPUT);
  pinMode(pin5, OUTPUT);
  pinMode(pin6, OUTPUT);
  pinMode(pin7, OUTPUT);
}

void loop(){
  unsigned long currentMillis = millis();
  if(motorState == LOW){
    
    //////////////////////Code to Stop the Motors
      digitalWrite(pin0, LOW); // First Step
      digitalWrite(pin4, LOW);
      digitalWrite(pin1, LOW);
      digitalWrite(pin5, LOW);
      digitalWrite(pin2, LOW);
      digitalWrite(pin6, LOW);
      digitalWrite(pin3, LOW);
      digitalWrite(pin7, LOW);
      delay(motorDelayTime);
    //////////////////////Code to Stop the Motors
    
    ////////////////////////Code to Read and Display Temperature and Humidity
    DHT22_ERROR_t errorCode;
    delay(2000);

    errorCode = myDHT22.readData();
    switch(errorCode)
    {
    case DHT_ERROR_NONE:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Temp: ");
      lcd.setCursor(6, 0);
      lcd.print(myDHT22.getTemperatureC());
      lcd.setCursor(12, 0);
      lcd.print((char)223);
      lcd.setCursor(13, 0);
      lcd.print("C");

      lcd.setCursor(0, 1);
      lcd.print("Humed: ");
      lcd.setCursor(7, 1);
      lcd.print(myDHT22.getHumidity());
      lcd.setCursor(13, 1);
      lcd.print("%RH");
      delay(1000);
      break;

    case DHT_ERROR_CHECKSUM:
      lcd.setCursor(0, 0);
      lcd.print("Erro Sum ");
      lcd.setCursor(9, 0);
      lcd.print(myDHT22.getTemperatureC());
      lcd.setCursor(13, 0);
      lcd.print("C");
      Serial.print(myDHT22.getHumidity());
      Serial.println("%RH");
      break;
    case DHT_BUS_HUNG:
      lcd.setCursor(0, 0);
      lcd.println("BUS Hung        ");
      break;
    case DHT_ERROR_NOT_PRESENT:
      lcd.setCursor(0, 0);
      lcd.println("Not Present     ");
      break;
    case DHT_ERROR_ACK_TOO_LONG:
      lcd.setCursor(0, 0);
      lcd.println("ACK time out    ");
      break;
    case DHT_ERROR_SYNC_TIMEOUT:
      lcd.setCursor(0, 0);
      lcd.println("Sync Timeout    ");
      break;
    case DHT_ERROR_DATA_TIMEOUT:
      lcd.setCursor(0, 0);
      lcd.println("Data Timeout    ");
      break;
    case DHT_ERROR_TOOQUICK:
      lcd.setCursor(0, 0);
      lcd.println("Polled to quick ");
      break;
    }
    ////////////////////////Code to Read and Display Temperature and Humidity

    //////////////////Code to read the Sensor PIR and alarm when a chicken is born
    int buttonState;
    buttonState = digitalRead(sensorPir);
    if (buttonState == HIGH) {
      lcd.clear();
      tone(buzzer, 2500, 1000);
      lcd.setCursor(0, 0);
      lcd.println("PINTO NASCEU    ");
      lcd.setCursor(0, 1);
      lcd.println("PEGUE SEU PINTO ");
      delay(1000);
    }
    else {
      digitalWrite(buzzer, LOW);
    }
    //////////////////Code to read the Sensor PIR and alarm when a chicken is born

    //////////////////////Code to control Humidity
     if(myDHT22.getHumidity() >= 70){
     digitalWrite(fan, HIGH);
     }else
     if(myDHT22.getHumidity() < 70 && myDHT22.getHumidity() > 60){
     digitalWrite(fan, LOW);
     }else
     if(myDHT22.getHumidity() <= 60){
     lcd.clear();
     tone(buzzer, 2500, 1000);
     lcd.setCursor(0, 0);
     lcd.println("NIVEL AGUA BAIXO");
     lcd.setCursor(0, 1);
     lcd.println("COLOCAR AGUA    ");
     delay(1000);
     }
    //////////////////////Code to control Humidity

    //////////////////////Code to control Temperature
    if(myDHT22.getTemperatureC() < 37){
      lcd.clear();
      tone(buzzer, 2500, 1000);
      lcd.setCursor(0, 0);
      lcd.println("TEMP. MTO BAIXA ");
      lcd.setCursor(0, 1);
      lcd.println("VER RESISTENCIA ");
      delay(1500);
    }
    else
      if(myDHT22.getTemperatureC() < 37.5) {
        digitalWrite(resistor, HIGH);
      }
      else
        if(myDHT22.getTemperatureC() >= 37.5){
          digitalWrite(resistor, LOW);
        }
    //////////////////////Code to control Temperature

    /////////////////////////////Code control motor spin
    if(currentMillis - previousMillis > 21600000){ //4 horas = 1440000, 6 horas = 21600000, 8 horas = 28800000, 12 horas =  43200000
      motorState = HIGH;
      previousMillis = currentMillis;
    }
  }
  else{
    lcd.setCursor(0, 0);
    lcd.println("MOTOR RODANDO   ");
    lcd.setCursor(0, 1);
    lcd.println("MOTOR RODANDO   ");
    for(int i = 0; i < 260; i++){
      //////////////////////Code to Move the Motors
      digitalWrite(pin0, HIGH); // First Step
      digitalWrite(pin4, HIGH);
      digitalWrite(pin1, LOW);
      digitalWrite(pin5, LOW);
      digitalWrite(pin2, LOW);
      digitalWrite(pin6, LOW);
      digitalWrite(pin3, LOW);
      digitalWrite(pin7, LOW);
      delay(motorDelayTime);

      digitalWrite(pin0, LOW); // Second Step
      digitalWrite(pin4, LOW);
      digitalWrite(pin1, HIGH);
      digitalWrite(pin5, HIGH);
      digitalWrite(pin2, LOW);
      digitalWrite(pin6, LOW);
      digitalWrite(pin3, LOW);
      digitalWrite(pin7, LOW);
      delay(motorDelayTime);

      digitalWrite(pin0, LOW); // Third Step
      digitalWrite(pin4, LOW);
      digitalWrite(pin1, LOW);
      digitalWrite(pin5, LOW);
      digitalWrite(pin2, HIGH);
      digitalWrite(pin6, HIGH);
      digitalWrite(pin3, LOW);
      digitalWrite(pin7, LOW);
      delay(motorDelayTime);

      digitalWrite(pin0, LOW); // Fourth Step
      digitalWrite(pin4, LOW);
      digitalWrite(pin1, LOW);
      digitalWrite(pin5, LOW);
      digitalWrite(pin2, LOW);
      digitalWrite(pin6, LOW);
      digitalWrite(pin3, HIGH);
      digitalWrite(pin7, HIGH);
      delay(motorDelayTime);
      //////////////////////Code to Move the Motors
    }
    if(motorState == HIGH){
      if(currentMillis - previousMillis > 6000){
        previousMillis = currentMillis;
        motorState = LOW;
      }
    }
    /////////////////////////////Code control motor spin
  }
}




