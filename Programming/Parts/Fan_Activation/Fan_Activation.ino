#define RELAY1  6                        

 
void setup()
{    
// Initialise the Arduino data pins for OUTPUT
  pinMode(RELAY1, OUTPUT);       
}
 
 void loop()
{
  while (1){
   digitalWrite(RELAY1,LOW);           // Turns ON Relays 1
   delay(5000);                                      // Wait 2 seconds
   digitalWrite(RELAY1,HIGH);         // Turns Relay Off
   delay(3000);
  }
  
}
