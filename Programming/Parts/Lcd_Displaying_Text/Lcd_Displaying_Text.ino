// include the libraries
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 10, 8, 9, 7);
int contrast = 6;

void setup()
{

  
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  pinMode(contrast, OUTPUT);
  
}

void loop()
{
   contrast = HIGH;
   lcd.setCursor(0, 0);
   lcd.print("Rogerio Mais");
   lcd.setCursor(0, 2);
   lcd.print("Marica");
   delay(200);
}
