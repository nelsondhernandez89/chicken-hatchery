#include <LiquidCrystal.h>
#include <DHT22.h>
#include <stdio.h>
#define DHT22_PIN 2
#define RELAY1  6      

// Setup a DHT22 instance
DHT22 myDHT22(DHT22_PIN);

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 10, 8, 9, 7);

void setup()
{
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);  
  pinMode(RELAY1, OUTPUT);    
  
}

void loop()
{
  ///////////////////Code to Read Temperature and Humidity Sensor
  DHT22_ERROR_t errorCode;
  delay(2000);

  errorCode = myDHT22.readData();
  switch(errorCode)
  {
    case DHT_ERROR_NONE:
      lcd.setCursor(0, 0);
      lcd.print("Temp: ");
      lcd.setCursor(6, 0);
      lcd.print(myDHT22.getTemperatureC());
      lcd.setCursor(12, 0);
      lcd.print((char)223);
      lcd.setCursor(13, 0);
      lcd.print("C");
      Serial.println(myDHT22.getTemperatureC());
      
      lcd.setCursor(0, 1);
      lcd.print("Humed: ");
      lcd.setCursor(7, 1);
      lcd.print(myDHT22.getHumidity());
      lcd.setCursor(13, 1);
      lcd.print("% RH");
      Serial.println(myDHT22.getHumidity());
      break;
      
    case DHT_ERROR_CHECKSUM:
      lcd.setCursor(0, 0);
      lcd.print("Erro Sum ");
      lcd.setCursor(9, 0);
      lcd.print(myDHT22.getTemperatureC());
      lcd.setCursor(13, 0);
      lcd.print("C");
      Serial.print(myDHT22.getHumidity());
      Serial.println("% RH");
      break;
    case DHT_BUS_HUNG:
    lcd.setCursor(0, 0);
      lcd.println("BUS Hung ");
      break;
    case DHT_ERROR_NOT_PRESENT:
      lcd.setCursor(0, 0);
      lcd.println("Not Present ");
      break;
    case DHT_ERROR_ACK_TOO_LONG:
      lcd.setCursor(0, 0);
      lcd.println("ACK time out ");
      break;
    case DHT_ERROR_SYNC_TIMEOUT:
      lcd.setCursor(0, 0);
      lcd.println("Sync Timeout ");
      break;
    case DHT_ERROR_DATA_TIMEOUT:
      lcd.setCursor(0, 0);
      lcd.println("Data Timeout ");
      break;
    case DHT_ERROR_TOOQUICK:
      lcd.setCursor(0, 0);
      lcd.println("Polled to quick ");
      break;
  }
 ///////////////////Code to Read Temperature and Humidity Sensor
 
 if (myDHT22.getHumidity() > 75){
   digitalWrite(RELAY1,LOW);
 } else {
   digitalWrite(RELAY1,HIGH); 
 }
 
 if (myDHT22.getTemperatureC()> 30){
   digitalWrite(RELAY1,LOW);
 } else {
   digitalWrite(RELAY1,HIGH); 
 }
  
}
