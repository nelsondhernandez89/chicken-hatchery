#include <stdio.h>

//Define Pines Motor 2
int pin0 = A0;
int pin1 = A1;
int pin2 = A2;
int pin3 = A3;
//Define Pines Motor 1
int pin4 = A4;
int pin5 = A5;
int pin6 = 0;
int pin7 = 1;
int motorDelayTime = 3;

void setup()
{
  //Define Analog Pins as Digital Outputs Motor 1
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  
  //Define Analog Pins as Digital Outputs Motor 2
  pinMode(pin4, OUTPUT); 
  pinMode(pin5, OUTPUT);
  pinMode(pin6, OUTPUT);
  pinMode(pin7, OUTPUT);
  
}

void loop()
{  
    //////////////////////Code to Move the Motors
  digitalWrite(pin0, HIGH); // First Step
  digitalWrite(pin4, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin5, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin3, LOW);
  digitalWrite(pin7, LOW);
  delay(motorDelayTime);
  
  digitalWrite(pin0, LOW); // Second Step
  digitalWrite(pin4, LOW);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin5, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin3, LOW);
  digitalWrite(pin7, LOW);
  delay(motorDelayTime);
  
  digitalWrite(pin0, LOW); // Third Step
  digitalWrite(pin4, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin5, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin6, HIGH);
  digitalWrite(pin3, LOW);
  digitalWrite(pin7, LOW);
  delay(motorDelayTime);
  
  digitalWrite(pin0, LOW); // Fourth Step
  digitalWrite(pin4, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin5, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin6, LOW);
  digitalWrite(pin3, HIGH);
  digitalWrite(pin7, HIGH);
  delay(motorDelayTime);
 //////////////////////Code to Move the Motors  
}
