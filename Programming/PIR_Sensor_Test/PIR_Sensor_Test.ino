int sensorPir = 3;
int buzzer =  5; 
 
int buttonState;
 
void setup() {
  pinMode(buzzer, OUTPUT);      
  pinMode(sensorPir, INPUT);     
  Serial.begin(9600);
}
 
void loop(){
  buttonState = digitalRead(sensorPir);
  if (buttonState == HIGH) {     
     tone(buzzer, 2500, 200);
     //Retardo
     delay(100);
  } 
  else {
    digitalWrite(buzzer, LOW); 
  }
  
  Serial.println(buttonState);
  
  delay(500);
}
